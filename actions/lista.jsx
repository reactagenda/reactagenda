import Dispatcher from '../dispatchers/Dispatcher.jsx';

const addItem = item => {
	Dispatcher.dispatch({
		actionType: 'ADD_ITEM',
		item
	});
};

const updateItem = item => {
	Dispatcher.dispatch({
		actionType: 'UPDATE_ITEM',
		item
	});
};

const removeItem = index => {
	Dispatcher.dispatch({
		actionType: 'REMOVE_ITEM',
		index
	});
};

export {
	addItem,
	updateItem,
	removeItem
}