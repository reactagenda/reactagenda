var webpack = require('webpack');
var path = require('path');

var parentDir = path.join(__dirname, '../');

module.exports = {
    watch: true,
    entry: parentDir + 'index.jsx',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },{
                test: /\.scss/,
                loaders: ["style-loader", "css-loder", "sass-loader"]
            }
        ]
    },
    output: {
        path: parentDir + 'dist',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: parentDir,
        historyApiFallback: true,
    },
    mode: 'development',
}
