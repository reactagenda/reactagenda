import React, { Component } from 'react';

export default class ContactNone extends Component {

	render () {
		return (
			<div className="mdl-cell mdl-cell--7-col mdl-cell--12-col-phone">
				<div className="contact-information">
					<div><span className="icon-icon-agenda big-big"></span></div>
				</div>
			</div>
		);
	}
}
