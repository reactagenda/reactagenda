import React, { Component } from 'react';
import Header from './Header.jsx';
import Main from './Main.jsx';
import { BrowserRouter as Router } from 'react-router-dom';

export default class App extends Component {

    render () {
        return (
        	<Router>
				<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header body-book">
					<Header/>
					<Main/>
				</div>
			</Router>
        );
    }
}
