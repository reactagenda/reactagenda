import React, { Component } from 'react';
import ListStore from '../stores/ListStore.jsx';
import { Redirect } from 'react-router-dom';
import ButtonSaveContact from './ButtonSaveContact.jsx';
import ButtonRemoveContact from './ButtonRemoveContact.jsx';

export default class Form extends Component {
	constructor(props){
		super(props);

		const item = this.getItemById(props);

		this.state = {
			nameValue: item.nameValue || "",
			cellValue: item.cellValue || "",
			emailValue: item.emailValue || "",
			addressValue: item.addressValue || "",
			lastNameValue: item.lastNameValue || "",
			redirect: false
		};

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	getItemById(props){
		if (typeof props.id == 'undefined') {
			return {};
		}

		return ListStore.getItemByIndex(props.id);
	}

	handleSubmit(event) {
		event.preventDefault();

		const { onSubmit } = this.props;

		const values = Object.assign({}, this.state);

		onSubmit(values);

		this.setState({redirect: true});
	}

	shouldComponentUpdate(nextProps, nextState){
		if (
			nextState.nameValue == this.state.nameValue &&
			nextState.cellValue == this.state.cellValue &&
			nextState.emailValue == this.state.emailValue &&
			nextState.addressValue == this.state.addressValue &&
			nextState.lastNameValue == this.state.lastNameValue
		) {
			return false
		}

		return true;
	}

	render () {
		const { nameValue, lastNameValue, cellValue, emailValue, addressValue, redirect } = this.state;
		const { onSubmit, onDelete, id } = this.props;

		if (redirect) {
			return <Redirect push to="/" />;
		}

		return (
			<div className="mdl-cell mdl-cell--7-col mdl-cell--12-col-phone">
				<div className="contact-information">
					<form>
						<div className="contact-name">
							<i className="material-icons prefix big">account_circle</i>
							<div className=" mdl-textfield mdl-js-textfield mdl-textfield--floating-label input-field">
								<input
									className="mdl-textfield__input"
									type="text"
									id="nome"
									value={ nameValue }
									onChange={(event => {this.setState({ nameValue: event.target.value })})}
								/>
								<label className="mdl-textfield__label" htmlFor="nome">Nome</label>
							</div>
							<div className=" mdl-textfield mdl-js-textfield mdl-textfield--floating-label input-field">
								<input
									className="mdl-textfield__input"
									type="text"
									id="sobrenome"
									value={ lastNameValue }
									onChange={(event => {this.setState({ lastNameValue: event.target.value })})}
								/>
								<label className="mdl-textfield__label" htmlFor="sobrenome">Sobrenome</label>
							</div>
						</div>

						<div className="contact-data">
							<div className="contact-name-sm">
								<div className=" mdl-textfield mdl-js-textfield mdl-textfield--floating-label input-field">
									<i className="material-icons prefix">account_circle</i>
									<input
										className="mdl-textfield__input"
										type="text"
										id="nome"
										value={ nameValue }
										onChange={(event => {this.setState({ nameValue: event.target.value })})}
									/>
									<label className="mdl-textfield__label" htmlFor="nome">Nome</label>
								</div>
								<br/>
								<div className=" mdl-textfield mdl-js-textfield mdl-textfield--floating-label input-field">
									<i className="material-icons prefix">account_circle</i>
									<input
										className="mdl-textfield__input"
										type="text"
										id="sobrenome"
										value={ lastNameValue }
										onChange={(event => {this.setState({ lastNameValue: event.target.value })})}
									/>
									<label className="mdl-textfield__label" htmlFor="sobrenome">Sobrenome</label>
								</div>
							</div>
							<div className=" mdl-textfield mdl-js-textfield mdl-textfield--floating-label input-field">
								<i className="material-icons prefix phone">phone</i>
								<input
									className="mdl-textfield__input"
									type="tel"
									pattern="-?[0-9]*(\.[0-9]+)?"
									id="telefone"
									value={ cellValue }
									onChange={(event => {this.setState({ cellValue: event.target.value })})}
									/>
								<label className="mdl-textfield__label" htmlFor="telefone">Telefone</label>
								<span className="mdl-textfield__error">Digite apenas numeros!</span>
								<button className="mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored plus">
									<i className="material-icons">add</i>
								</button>
							</div>
							<br/>
							<div className=" mdl-textfield mdl-js-textfield mdl-textfield--floating-label input-field">
								<i className="material-icons prefix">email</i>
								<input
									className="mdl-textfield__input"
									type="email"
									id="email"
									value={ emailValue }
									onChange={(event => {this.setState({ emailValue: event.target.value })})}
									/>
								<label className="mdl-textfield__label" htmlFor="email">E-mail</label>
							</div>
							<br/>
							<div className=" mdl-textfield mdl-js-textfield mdl-textfield--floating-label input-field">
								<i className="material-icons prefix">place</i>
								<input
									className="mdl-textfield__input"
									type="text"
									id="endereco"
									value={ addressValue }
									onChange={(event => {this.setState({ addressValue: event.target.value })})}
									/>
								<label className="mdl-textfield__label" htmlFor="endereco">Endereço</label>
							</div>
						</div>
						<ButtonRemoveContact onDelete={onDelete} id={id}/>
						<ButtonSaveContact onSubmit={onSubmit} data={this.state}/>
					</form>
				</div>
			</div>
		);
	}
}
