import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

export default class ButtonRemoveContact extends Component {
	constructor(props){
		super(props);

		this.state = {
			redirect: false
		};

		this.onDelete = this.onDelete.bind(this);
	}

	onDelete(event) {
		event.preventDefault();

		const { onDelete, id } = this.props;

		onDelete(id);

		this.setState({redirect: true});
	}

	render () {
		const { redirect } = this.state;
		const { id } = this.props;

		if (redirect) {
			return <Redirect push to="/" />;
		}

		if (typeof id == 'undefined') {
			return " ";
		}

		return (
			<button className="delete" onClick={ this.onDelete }>
				<i className="material-icons delete-icon">delete_outline</i>
				Excluir
			</button>
		);
	}
}
