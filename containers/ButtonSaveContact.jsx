import React, { Component } from 'react';
import ListStore from '../stores/ListStore.jsx';
import { Redirect } from 'react-router-dom';

export default class ButtonSaveContact extends Component {
	constructor(props){
		super(props);

		this.state = {
			redirect: false,
			button: '',
			data: this.props.data
		};

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(event) {
		event.preventDefault();

		const { onSubmit } = this.props;

		const values = Object.assign({}, this.props.data);

		onSubmit(values);

		this.setState({redirect: true});
	}

	componentWillReceiveProps(nextProps){
		if (
			nextProps.data.nameValue == this.state.data.nameValue &&
			nextProps.data.cellValue == this.state.data.cellValue &&
			nextProps.data.emailValue == this.state.data.emailValue &&
			nextProps.data.addressValue == this.state.data.addressValue &&
			nextProps.data.lastNameValue == this.state.data.lastNameValue
		) {
			this.setState({
				button: ''
			});
			return;
		}


		this.setState({
			button: <button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored save" onClick={ this.handleSubmit }>
					Salvar
				</button>
		});
	}

	render () {
		const { redirect, button } = this.state;

		if (redirect) {
			return <Redirect push to="/" />;
		}

		return ( button	);
	}
}
