import React, { Component } from 'react';
import ItemLista from './ItemLista.jsx';

export default class Lista extends Component {

	renderItens(){
    	const { itens } = this.props;

		return itens.map((item, index) => {
			return (
				<ItemLista
					key={index}
					link={"/Form/" + index}
					item={ item }
				/>
			);
		});
	}

    render () {
    	const { itens } = this.props;

    	if(itens.length == 0){
    		return (
    			<div className="people">
					<nav className="items-people">
						<p>Nenhum item na lista</p>
					</nav>
				</div>
			);
    	}

        return (
			<div className="people">
				<nav className="items-people">
					{this.renderItens()}
				</nav>
			</div>
        );
    }
}
