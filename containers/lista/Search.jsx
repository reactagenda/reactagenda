import React, { Component } from 'react';

export default class Search extends Component {
	constructor(props){
		super(props);
	}

    render () {
        return (
			<div className="search">
				<div className="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
					<label className="mdl-button mdl-js-button mdl-button--icon" htmlFor="pesquisa">
						<i className="material-icons">search</i>
					</label>
					<div className="mdl-textfield__expandable-holder">
						<input className="mdl-textfield__input" type="text" id="pesquisa"/>
						<label className="mdl-textfield__label" htmlFor="sample-expandable">Expandable Input</label>
					</div>
				</div>
			</div>
        );
    }
}
