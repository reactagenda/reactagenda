import React, { Component } from 'react';
import { addItem, removeItem, updateItem } from '../../actions/lista.jsx';
import ListStore from '../../stores/ListStore.jsx';
import Search from './Search.jsx';
import Lista from './Lista.jsx';
import ButtonNew from './ButtonNew.jsx';

export default class ContactList extends Component {
	constructor(props){
		super(props);

		this.state = {
			lista: ListStore.getItens()
		};

		this.onListChange = this.onListChange.bind(this);
	}

	componentDidMount(){
		ListStore.on('change', this.onListChange);
	}

	componentWillUnmount(){
		ListStore.removeListener('change', this.onListChange);
	}

	onListChange(lista){
		this.setState({ lista });
	}

    render () {
    	const { nome, lista } = this.state;

        return (
        	<div className="mdl-cell mdl-cell--5-col mdl-cell--12-col-phone">
				<div className="contacts-list">
					<Search/>
					<Lista
						itens={lista}
					/>
					<ButtonNew/>
				</div>
			</div>
        );
    }
}
