import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ButtonNew extends Component {

    render () {
        return (
			<Link to={'/Form'}>
				<button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--accent">
					<i className="material-icons">add</i>
				</button>
			</Link>
        );
    }
}
