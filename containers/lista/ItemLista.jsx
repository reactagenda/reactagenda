import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ItemLista extends Component {

	getInitials(){
		const { item } = this.props;

		return [ item.nameValue, item.lastNameValue ].map(n =>n[0]).join("");
	}

    render () {
    	const { item, onDelete, link } = this.props;

        return (
			<Link to={link} className="nav-link"><div className="iniciais-list">{this.getInitials()}</div>{item.nameValue}</Link>
        );
    }
}
