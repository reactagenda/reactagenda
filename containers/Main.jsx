import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import ContactList from './lista/ContactList.jsx';
import Form from './Form.jsx';
import ContactNone from './ContactNone.jsx';
import { addItem, removeItem, updateItem } from '../actions/lista.jsx';

export default class Main extends Component {

    render () {
        return (
        	<main className="mdl-layout__content">
				<div className="mdl-grid">
					<ContactList/>
					<Switch>
						<Route exact path='/'>
							<ContactNone/>
						</Route>
						<Route exact path='/Form'>
							<Form onSubmit={addItem} />
						</Route>
						<Route exact path='/Form/:id' component={(props) =>
							<Form
								id={props.match.params.id}
								onSubmit={updateItem}
								onDelete={removeItem}
							/>
						}/>
					</Switch>

				</div>
			</main>
        );
    }
}
