import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component {

    render () {
        return (
        	<header className="mdl-layout__header header-style">
				<div className="mdl-layout__header-row">
					<h1 className="mdl-layout-title">
						<Link to={'/'}>Agenda <i className="icon-icon-agenda"></i></Link>
					</h1>
				</div>
			</header>
        );
    }
}
