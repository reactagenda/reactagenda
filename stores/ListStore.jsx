import { EventEmitter } from 'events';
import Dispatcher from '../dispatchers/Dispatcher.jsx';
import persist from './persist.jsx';

let _itens = [];

let v = 1;

const addItem = item => {
	_itens.push(item);

	return persist.setItem('itens', _itens).then(result => {
		window.localStorage.setItem('itens', ++v);
		return result;
	});
};

const removeItem = index => {
	_itens.splice(index, 1);

	return persist.setItem('itens', _itens).then(result => {
		window.localStorage.setItem('itens', ++v);
		return result;
	});
};

const updateItem = item => {
	_itens.splice(item.index, 1);

	delete item.index;

	_itens.push(item);

	return persist.setItem('itens', _itens).then(result => {
		window.localStorage.setItem('itens', ++v);
		return result;
	});
};


class ListStore extends EventEmitter{
	constructor(){
		super();

		Dispatcher.register(payload => {
			const { index, item } = payload;

			switch(payload.actionType){
				case 'ADD_ITEM':
					addItem(item);

					this.emit('change', _itens);
					break;
				case 'REMOVE_ITEM':
					removeItem(index);

					this.emit('change', _itens);
					break;
				case 'UPDATE_ITEM':
					updateItem(item);

					this.emit('change', _itens);
					break;
			}
		});

		persist.getItem('itens').then(itens => {
			_itens = itens || [];

			this.emit('change', _itens);
		});

		window.addEventListener('storage', event => {
			persist.getItem('itens').then(itens => {
				_itens = itens || [];

				this.emit('change', _itens);
			});
		}, false);
	}

	getItens(){
		return _itens;
	}

	getItemByIndex(index){
		return _itens[index];
	}
}

export default new ListStore;