import localforage from 'localforage';

const store = localforage.createInstance({
  name: "nameHere"
});

store.config({
    name: 'lista',
    version: 1.0,
    description : 'Dummy list'
});

export default store;